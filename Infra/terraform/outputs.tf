### Gerando arquivo de inventário ansible de forma automatizada utilizando função templatefile
resource "local_file" "AnsibleInventory" {
 content = templatefile("inventory.tmpl",
 {
  master-dns = aws_instance.master.*.public_dns,
  master-ip = aws_instance.master.*.public_ip,
  master-private-ip = aws_instance.master.*.private_ip,
  master-id = aws_instance.master.*.id
  worker-dns = aws_instance.worker.*.public_dns,
  worker-ip = aws_instance.worker.*.public_ip,
  worker-private-ip = aws_instance.worker.*.private_ip,
  worker-id = aws_instance.worker.*.id,
  runner-dns = aws_instance.runner.*.public_dns,
  runner-ip = aws_instance.runner.*.public_ip,
  runner-private-ip = aws_instance.runner.*.private_ip,
  runner-id = aws_instance.runner.*.id,
  haproxy-dns = aws_instance.haproxy.*.public_dns,
  haproxy-ip = aws_instance.haproxy.*.public_ip,
  haproxy-private-ip = aws_instance.haproxy.*.private_ip,
  haproxy-id = aws_instance.haproxy.*.id
 }
 )
 filename = "inventory"
}
import os

mongodb_uri = os.getenv('MONGO_URI')

class Config(object):
    DEBUG = False
    TESTING = False
    
class ProdConfig(Config):
    ENV = 'Production'
    MONGO_URI = (f'mongodb://{mongodb_uri}:27017/myDatabase')
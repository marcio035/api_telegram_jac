# Infraestrutura
## Cloud AWS

![image](img/projeto.png)

### Sistema Operacional

* Ubuntu 20.04

## Ferramentas utilizadas para provisionamento e configuração

1. Realizar o deploy das VMs EC2 na AWS de forma automatizada utilizando Terraform.
* [Documentação](../Infra/terraform/README.md)
2. Utilizar ansible para realizar as configurações do ambiente.
* [Documentação](../Infra/ansible/README.md)

## API
* API python utilzando flask
* [Documentação](../app/README.md)
##

## CICD Gitlab
* O pipeline Gitlab possue 3 stages:
1. build, constroi a image docker da API e envia para o docker hub.
2. info, realizar um kubectl get nodes para verificar status do cluster.
3. deploy, realiza um kubectl apply nos manifestos da API.

* Para execução deve-se add como variáveis de ambiente no repo do projeto usuário e senha do docker hub e KUBE_CONFIG em base64